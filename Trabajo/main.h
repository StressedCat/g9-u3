#include <fstream>
#include <iostream>
using namespace std;

/* estructura del nodo */
typedef struct _Hash {
    int value;
    int key;
    struct _Hash *next;
} Hash;

/* se crea un nuevo Nodo */
Hash *crearHash(int value, int key) {
    Hash *q;
    q = new Hash();
    q->next = NULL;
    q->value = value;
    q->key = key;
    return q;
}
