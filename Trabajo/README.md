# G9-U3

# Requisitos:  
C++  
Para iniciar el programa:  
1.- Ingrese en el CMD el comando make para instalar el programa  
-> .../g9-u3/Trabajo$ make  
2.- Se creará un programa con nombre de Cargo, para ingresar a este
haga lo siguiente:  
-> .../g9-u3/Trabajo$ ./hash  

# En el programa:
El programa consiste en una forma de buscar elementos llamada "hash", esta
forma de buscar es mas veloz que otras formas, ya que es directa y no necesita
la lista ordenada, pero pueden ocurrir errores de colisión, el programa
debe tener formas de tratar esto, estas siendo Linea, Cuadrática, Doble dirección
y Encadenamiento.

# Para borrar el programa:
Asegurese que esté en la misma carpeta donde corrio el programa
-> .../g9-u3/Trabajo  
Ingrese el comando make clean para borrar el programa que uso
-> .../g9-u3/Trabajo$ make clean
