#include <fstream>
#include <iostream>
#include <cstdio>
using namespace std;
#include "main.h"


/* Mostrar el hash por completo con su key y su valor correspondiente */
void showHash(Hash *hash){
  if (hash->key != 0) {
    cout << "Key: " << hash->key << endl;
    cout << "Value: " << hash->value << endl;
  }
  if (hash->next != NULL) {
    showHash(hash->next);
  }
}

/* Añadir el Hash dependiendo de su key, con la condición de que esta key sea unica */
bool AddHash(Hash *hash, int key, int value, int *list, int choice){
  /* ir al siguiente spot */
  if (hash->next != NULL) {
    if (hash->key != key && hash->next->key != key) {
      AddHash(hash->next, key, value, list, choice);
    }
    /* la key no es unica, hay colisión o es el valor*/
    else{
      if (hash->value == value || hash->next->value == value) {
        cout << "El valor ya se encuentra ingresado" << endl;
        if (choice != 3) {
          return true;
        }
        else{
          return false;
        }
      }
      else{
        cout << "Colisión" << endl;
        return false;
      }
    }
  }
  /* la key es unica, se agrega el valor */
  else{
    cout << "se agrega el valor en la key: " << key << endl;
    hash->next = crearHash(value, key);
    if (choice != 4) {
      list[key] = value;
    }
    /* se devuelve true para mostrar que la key si existió */
    return true;
  }
}

void SearchHash(Hash *hash, int value){
  if (hash->value == value) {
    cout << "La key del valor es: " << hash->key << endl;
  }
  else if (hash->next != NULL) {
    SearchHash(hash->next, value);
  }
  else{
    cout << "el valor no se encuentra en el listado" << endl;
  }
}

int main() {
   Hash *info = NULL;
   Hash *info2 = NULL;
   int k;
   int v;
   int c;
   int x;
   bool flick = false;
   bool action;
   info = crearHash(0, 0);
   info2 = crearHash(0, 0);
   int list[100];
   int list2[100];
   list[0] = 0;
   list2[0] = 0;
   int choice;
   for (int y = 0; y < 100; y++) {
     list[y] = 0;
     list2[y] = 0;
   }

   /* Se le da al usuario la eleccion de que metodo quiere usar */
   cout << "Cual metodo desea utilizar para hacer la lista" << endl;
   while (choice <= 0 || choice >= 5) {
     cout << "[1] Lineal" << endl << "[2] Cuadrática" <<  endl;
     cout << "[3] Doble Dirección" << endl << "[4] Encadenamiento" << endl;
     cout << "Ingrese su elección" << endl;
     cin >> choice;
   }

   /* Menu para las operaciones */
   while (flick != true) {
      cout << "[1] Ingresar nuevo valor"<<endl;
      cout << "[2] Ver valores" << endl;
      cout << "[3] Buscar un valor" << endl;
      cout << "[0] Terminar el programa" << endl;
      cout << "Ingrese su eleccion: " << endl;
      cin >> c;

      /* Nuevo valor para el hash */
      if(c == 1){
        action = false;
        cout << "Ingrese el valor" << endl;
        v = 0;
        while(v <= 0 || v >= 10000){
          cout << "el valor debe ser mayor a 0 y menor a 10000" << endl;
          cin >> v;
        }
        cout << endl;

        /* Forma Lineal */
        if (choice == 1) {
          cout << "=== Lineal ===" << endl;
          x = 1;
          while (action != true) {
            if (((v % 100) + x) < 100) {
              k = (v % 100) + x;
              cout << "Key: " << k << endl;
              action = AddHash(info, k, v, list, choice);
              x = x + 1;
            }
            else{
              cout << "El valor no pudo ser agregado" << endl;
              action = true;
            }
          }
          cout << "==============" << endl << endl;
        }

        /* Forma Cuadrática */
        if(choice == 2){
          cout << "=== Cuadrática ===" << endl;
          x = 1;
          while (action != true) {
            if (((v % 100) + x*x) < 100) {
              k = (v % 100) + x*x;
              cout << "Key: " << k << endl;
              action = AddHash(info, k, v, list, choice);
              x = x + 1;
            }
            else{
              cout << "El valor no pudo ser agregado" << endl;
              action = true;
            }
          }
          cout << "==================" << endl << endl;
        }

        /* Forma Doble Dirección */
        else if (choice == 3) {
          /* Se usarán dos listas, si una esta usada, se usará la otra, si está usada, se rechaza */
          cout << "=== Doble Dirección ===" << endl;
          k = (v%100) + 1;
          cout << "Key: " << k << endl;
          action = AddHash(info, k, v, list, choice);
          if (action != true) {
            action = AddHash(info2, k, v, list2, choice);
            if (action != true) {
              cout << "El valor no pudo ser agregado" << endl;
            }
            else{
              cout << "Fue agregado en la lista 2" << endl;
            }
          }
          else{
            cout << "Fue agregado en la lista 1" << endl;
          }
          cout << "=======================" << endl << endl;
        }

        /* Forma Encadenamiento */
        else if (choice == 4) {
          /* Todo será guardado en "nodos" */
          x = 1;
          cout << "=== Encadenamiento ===" << endl;
          while (action != true) {
            k = (v % 100) + x;
            cout << "Key: " << k << endl;
            action = AddHash(info, k, v, list, choice);
            x = x + 1;
          }
          cout << "======================" << endl << endl;
        }
      }

      /* Mostrar los valores que se tienen */
      else if(c == 2){
        /* Si es una lista singular, se hace normalmente */
        if (choice != 3) {
          showHash(info);
        }
        /* Si es una lista doble, se mostrará cada una */
        else{
          cout << "== Lista 1 ==" << endl;
          showHash(info);
          cout << "=============" << endl << endl;
          cout << "== Lista 2 ==" << endl;
          showHash(info2);
          cout << "=============" << endl << endl;
        }
      }

      /* Buscar un valor */
      else if(c == 3){
        cout << "Ingrese el valor que desea buscar" << endl;
        v = 0;
        while(v <= 0 || v >= 10000){
          cout << "el valor debe ser mayor a 0 y menor a 10000" << endl;
          cin >> v;
        }
        action = false;
        cout << endl;

        /* Forma Lineal */
        if (choice == 1) {
          x = 1;
          cout << "=== Lineal ===" << endl;
          while (action != true) {
            if (((v % 100) + x) < 100) {
              /* Se sumará 1 hasta encontrarla, si llega a mayor a 100, no está */
              k = (v % 100) + x;
              cout << "Key: " << k << endl;
              if (list[k] == v) {
                cout << "La key del valor es: " << k << endl;
                action = true;
              }
              else{
                if (list[k] != 0) {
                  cout << "Colisión" << endl;
                }
                else{
                  cout << "Vacio" << endl;
                }
              }
            }
            else{
              cout << "El valor no existe" << endl;
              action = true;
            }
            x = x + 1;
          }
          cout << "==============" << endl << endl;
        }

        /* Forma Cuadrática */
        else if (choice == 2) {
          x = 1;
          cout << "=== Cuadrática ===" << endl;
          while (action != true) {
            /* Se sumará 1 y se elevará a 2 hasta encontrarla
               si llega a mayor a 100, no está */
            if (((v % 100) + x*x) < 100) {
              k = ((v) % 100) + x*x;
              cout << "Key: " << k << endl;
              if (list[k] == v) {
                cout << "La key del valor es: " << k << endl;
                action = true;
              }
              else{
                if (list[k] != 0) {
                  cout << "Colisión" << endl;
                }
                else{
                  cout << "Vacio" << endl;
                }
              }
              x = x + 1;
            }
            else{
              cout << "El valor no existe" << endl;
              action = true;
            }
          }
          cout << "==================" << endl << endl;
        }

        /* Forma Doble Dirección */
        else if (choice == 3) {
          k = (v%100) + 1;
          cout << "Key: " << k << endl;
          cout << "=== Doble Dirección ===" << endl;
          /* Se busca en la lista 1 si está el valor */
          if (list[k] == v) {
            cout << "El valor esta en la lista 1" << endl;
            cout << "Se ubica en la key: " << k << endl;
          }
          /* Se busca en la lista 2 si está el valor */
          else if (list2[k] == v && list[k] != 0) {
            cout << "Colisión en lista 1" << endl;
            cout << "El valor esta en la lista 2" << endl;
            cout << "Se ubica en la key: " << k << endl;
          }
          /* Si no esta en ambas, no existe */
          else{
            if (list[k] != 0) {
              cout << "Colisión lista 1 y lista 2 vacia" << endl;
            }
            else if (list2[k] != 0) {
              cout << "Colisión en lista 1 y 2" << endl;
            }
            else{
              cout << "Listas vacias" << endl;
            }
            cout << "El valor no se encuentra en la listas" << endl;
          }
          cout << "=======================" << endl << endl;
        }

        /* Forma encadenamiento */
        else if (choice == 4) {
          cout << "=== Encadenamiento ===" << endl;
          SearchHash(info, v);
          cout << "======================" << endl << endl;
        }

      }

      /* Cerrar programa */
      else if(c == 0) {
        flick = true;
      }

      /* Ingreso incorrecto */
      else{
        cout << "Ingreso incorrecto" << endl;
      }

   }
   return 0;
}
